<?php
/**
 * @package Wanderlust
 * @version 0.1
 */
/*
Plugin Name: Wanderlust
Plugin URI: https://radicalphilosophy.com
Description: A plugin to facilitate the migration of the Radical
Philosophy website.
Author: Alex Sassmannshausen
License: GPL3+
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
Version: 0.1

Wanderlust is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or any
later version.

Wanderlust is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wanderlust. If not, see
https://www.gnu.org/licenses/gpl-3.0.en.html.
*/

// Preliminaries
defined( 'ABSPATH' ) or die( 'Fuck right off.' );

function wlst_activation ()
{
}
register_activation_hook( __FILE__, 'wlst_activation' );

function wlst_deactivation ()
{
}
register_deactivation_hook( __FILE__, 'wlst_deactivation' );

function wlst_uninstall ()
{
}
register_uninstall_hook( __FILE__, 'wlst_uninstall' );

function dossier_id ()
{
    return 10091;
}

function article_id ()
{
    return 4;
}

//// Functionality

// Add wanderlust page to tools menu
function wlst_options_page()
{
    add_management_page(
        'Wanderlust Migration Overview',
        'Wanderlust',
        'manage_options',
        'wanderlust',
        'wlst_html'
    );
}
add_action('admin_menu', 'wlst_options_page');

//// Helpers

/**
 * wlst_migrate
 *
 * Entry point for migration.
 *
 */
function wlst_migrate($categories, $dossiers)
{
    // We have following processes to carry out:
    // Category hygiene:
    //   1) Any content that have *just* the category dossier, need to
    //      be moved to 'article', except if it was created today
    //   2) Any content that have 'Dossier' and a dossier child, need to
    //      be moved to 'article' and that dossier child
    //   3) Any content that have the category 'Dossier' and other
    //      content should lose the 'Dossier' category
    // Generate our new category contents
    // Migrate to new category content
    //   1) Associate all old-school content category associations with their new counter part
    //   2) Remove their old-school linkage
    // Delete old dossier child categories

    $result = [
        "cat_hygiene" => [ "just_dossier" => [], "dossier_child" => [], "dossier_other" => [], "skipped" => [] ],
        "cat_generation" => [ "generated" => [], "skipped" => [] ],
        "posts_assoc" => [ "associated" => [], "cat_skipped" => [], "skipped" => [], "partial" => [] ],
        "cat_kill" => [ "killed" => [], "skipped" => [] ]
    ];
    $result = wlst_dossier_cat_hygiene($categories, $dossiers, $result);
    $result = wlst_dossier_cats_generate($categories, $result);
    $result = wlst_dossier_posts_associate($categories, $dossiers, $result);
    $result = wlst_dossier_children_kill($categories, $result);

    return $result;
}

/**
 * wlst_dossier_cat_issue
 *
 * Return the issue post that this $dossier_cat needs to be associated with.
 *
 */
function wlst_dossier_cat_issue($dossier_cat)
{
    $posts = get_posts([ 'name' => explode(" ", $dossier_cat->description)[1] ]);
    if ( count($posts) == 1 ) {
        return $posts[0];
    } elseif ( count($posts) == 0 ) {
        return 0;
    } else {
        die("Returned too many posts!");
    }
}

/**
 * wlst_dossier_cat_hygiene
 *
 * Manipulate the database such that none of our candidate posts contain edge
 * cases potentially breaking our migration.
 *
 */
function wlst_dossier_cat_hygiene($categories, $posts, $result)
{
    // We know there are no posts that contain more than 2 categories
    // Old dossier categories:
    $func = function($n) { return $n->term_id; };
    $cat_ids = array_map($func, $categories); // ID's of dossiers to migrate
    foreach ( $posts as $post ) {
        $post_cats = get_the_category($post->ID); // categories of post
        $post_date = explode(" ", $post->post_date)[0];
        $new_post = [
            'ID' => $post->ID,
            'post_status' => 'publish',
        ];
        $today = date("Y-m-d");
        if ( count($post_cats) == 1 && $post_cats[0]->term_id == dossier_id() && $post_date != $today ) {
            // Case (1): Only category dossier.
            // Set post_category to Article instead of dossier
            // This is to get rid of articles that are not really part of dossiers
            $new_post['post_category'] = [ article_id() ];
            $post_id = wp_update_post($new_post);
            $result['cat_hygiene']['just_dossier'][$post->ID] = $post->post_title;
        } elseif ( count($post_cats) == 2 && $post_cats[0]->term_id == dossier_id() ) {
            // Case (2) or (3)
            $search = array_search($post_cats[1]->term_id, $cat_ids);
            if ( $search ) {
                // Case (2)
                $new_post['post_category'] = [ $cat_ids[$search], article_id() ];
                $post_id = wp_update_post($new_post);
                $result['cat_hygiene']['dossier_child'][$post->ID] = $post->post_title;
            } else {
                // Case (3)
                $new_post['post_category'] = [ $post_cats[1]->term_id ];
                $post_id = wp_update_post($new_post);
                $result['cat_hygiene']['dossier_other'][$post->ID] = $post->post_title;
            }
        } elseif ( count($post_cats) == 2 && $post_cats[1]->term_id == dossier_id() ) {
            // Case (2) or (3)
            $search = array_search($post_cats[0]->term_id, $cat_ids);
            if ( $search ) {
                // Case (2)
                $new_post['post_category'] = [ $search, article_id() ];
                $post_id = wp_update_post($new_post);
                $result['cat_hygiene']['dossier_child'][$post->ID] = $post->post_title;
            } else {
                // Case (3)
                $new_post['post_category'] = [ $post_cats[0]->term_id ];
                $result['cat_hygiene']['dossier_other'][$post->ID] = $post->post_title;
            }
        } elseif ( count($post_cats) >= 3 ) {
            // Generic Case (3)
            $func = function($v) { return $v->term_id != dossier_id(); };
            $clean_cats = array_filter($post_cats, $func);
            $func = function($n) { return $n->term_id; };
            $new_post['post_category'] = array_map($func, $clean_cats);
            $post_id = wp_update_post($new_post);
            $result['cat_hygiene']['dossier_other'][$post->ID] = $post->post_title;
        } else {
            $result['cat_hygiene']['skipped'][$post->ID] = $post->post_title;
        }
    }
    return $result;
}

/**
 * wlst_dossier_cats_generate
 *
 * Search posts for each category in $CATEGORIES.  If no posts are found,
 * create a new post for category, else skip.
 *
 */
function wlst_dossier_cats_generate($categories, $result)
{
    foreach ( $categories as $category ) {
        // Our test is to see if we have any posts of the name of our
        // dossier category yet.
        $test = count(get_posts([ 'title' => $category->name, 'numberposts' => -1 ]));
        if ( $test == 0 ) {
            $post = [
                'post_title'    => $category->name,
                'post_status'   => 'publish',
                'post_category' => [ get_cat_id('dossier') ]
            ];
            $post_id = wp_insert_post($post);
            update_field(
                'field_5918591dacf66',
                wlst_dossier_cat_issue($category)->ID,
                $post_id
            );
            $result["cat_generation"]["generated"][$category->name] = $category->term_id;
        } elseif ( $test == 1 ) {
            $result["cat_generation"]["skipped"][$category->name] = $category->term_id;
        } else {
            die("We have somehow ended up with spare dossier posts!");
        }
    }
    return $result;
}

/**
 * wlst_dossier_posts_associate
 *
 * Associate each post in $POSTS currently part of a category with that
 * category's post, and dissociate it from its category.
 *
 */
function wlst_dossier_posts_associate($categories, $posts, $result)
{
    foreach ( $posts as $post ) {
        $post_cats = get_the_category($post->ID); // categories of post
        // Generate lists of ids to work with
        $func = function($n) { return $n->term_id; };
        $cat_ids = array_map($func, $categories);
        $post_cat_ids = array_map($func, $post_cats);
        // Locate ID to associate with
        $cat_post_ids = array_intersect($post_cat_ids, $cat_ids);
        // Do the work
        if ( count($cat_post_ids) == 1 ) {
            // We have the cat to associate with!
            $cat_id = array_pop($cat_post_ids);
            if ( get_field('dossier_title', $post->ID) ) {
                // We seem to have associated this already -> only remove category.
                $result['posts_assoc']['partial'][$post->ID] = $post->post_title;
            } else {
                // First associate, then remove category.
                $dossier_post = get_posts([ 'title' => get_category($cat_id)->name ])[0];
                update_field('field_591857e895c8b', $dossier_post->ID, $post->ID);
                $result['posts_assoc']['associated'][$post->ID] = $post->post_title;
            }
            // Remove category
            $new_post_cat_ids = array_diff($post_cat_ids, [ $cat_id ]);
            $post = [
                'ID' => $post->ID,
                'post_status' => 'publish',
                'post_category' => $new_post_cat_ids
            ];
            $post_id = wp_update_post($post);
        } elseif ( count($cat_post_ids) == 0 ) {
            if ( count($post_cat_ids) == 1 && $post_cat_ids[0] == dossier_id() ) {
                // => should be one of the 16 dossiers we just created
                $result['posts_assoc']['cat_skipped'][$post->ID] = $post->post_title;
            } else {
                $result['posts_assoc']['skipped'][$post->ID] = $post->post_title;
            }
        } else {
            die("We found more than 1 candidate dossier.  Should not happen!");
        }
    }
    return $result;
}

function wlst_dossier_children_kill($categories, $result) {
    foreach ( $categories as $category ) {
        $posts = get_posts([ 'category' => $category->term_id ]);
        if ( count($posts) == 0 && wp_delete_category($category->term_id) ) {
            $result['cat_kill']['killed'][$category->term_id] = $category->name;
        } else {
            $result['cat_kill']['skipped'][$category->term_id] = $category->name;
        }
    }
    return $result;
}

function wlst_cat_search($cat_id, $text)
{
    return "<a href=\"/wp-admin/edit.php?post_status=all&post_type=post&m=0&cat="
         . $cat_id . "&filter_action=Filter&paged=0\">" . $text . "</a>";
}

function wlst_post_search($post_id, $text)
{
    return "<a href=\"/wp-admin/post.php?post=" . $post_id . "&action=edit\">"
         . $text . "</a>";
}

// wlst_html
//
// Display the current status of the database and offer to start the migration.

function wlst_html()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
   ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <p>
            <em>
                This script can be run as any user, but it will set the
                dossiers that it creates to that user.  As a result I
                would recommend it is run from an unambiguously 'web-dev'
                account.
            </em>
        </p>
    <?php

    $categories = get_categories([ 'child_of' => get_cat_id('dossier'), 'hide_empty' => false ]);
    $dossiers = get_posts([ 'category' => get_cat_id('dossier'), 'numberposts' => -1 ]);

    // Basic operations
    if ( $_GET['operation'] == 'generate' ) {
        if ( count($categories) == 0 ) {
            die("Wanderlust need not be run.  Migration was already completed.");
        }
        $result = wlst_migrate($categories, $dossiers);
    ?>
        <h2>Result of migration</h2>
        <ol>
            <li>Dossier hygiene:
                <ol>
                    <li>Posts with only 'Dossier' category, now 'Article':
                        <ol>
                            <?php
                            foreach ( $result['cat_hygiene']['just_dossier'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts with only 'Dossier' & a dossier category child, now child & 'Article':
                        <ol>
                            <?php
                            foreach ( $result['cat_hygiene']['dossier_child'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts with only 'Dossier' & another type, now no longer 'Dossier':
                        <ol>
                            <?php
                            foreach ( $result['cat_hygiene']['dossier_other'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts that were skipped:
                        <ol>
                            <?php
                            foreach ( $result['cat_hygiene']['skipped'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                </ol>
            </li>
            <li>Dossier posts generation:
                <ol>
                    <li>Posts created:
                        <ol>
                            <?php
                            foreach ( $result['cat_generation']['generated'] as $name => $id ) {
                                echo "<li>" . wlst_cat_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts skipped:
                        <ol>
                            <?php
                            foreach ( $result['cat_generation']['skipped'] as $name => $id ) {
                                echo "<li>" . wlst_cat_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                </ol>
            </li>
            <li>Posts association:
                <ol>
                    <li>Posts completely associated:
                        <ol>
                            <?php
                            foreach ( $result['posts_assoc']['associated'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts partially merged:
                        <ol>
                            <?php
                            foreach ( $result['posts_assoc']['partial'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts completely skipped:
                        <ol>
                            <?php
                            foreach ( $result['posts_assoc']['skipped'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Posts skipped because they are our newly created categories:
                        <ol>
                            <?php
                            foreach ( $result['posts_assoc']['cat_skipped'] as $id => $name ) {
                                echo "<li>" . wlst_post_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                </ol>
            </li>
            <li>Old category extermination:
                <ol>
                    <li>Categories killed:
                        <ol>
                            <?php
                            foreach ( $result['cat_kill']['killed'] as $id => $name ) {
                                echo "<li>" . $name . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                    <li>Categories not killed:
                        <ol>
                            <?php
                            foreach ( $result['cat_kill']['skipped'] as $id => $name ) {
                                echo "<li>" . wlst_cat_search($id, $name) . "</li>";
                            }
                            ?>
                        </ol>
                    </li>
                </ol>
            </li>
        </ol>
    <?php
    }

    //// Basic statistics:

    // How many dossier categories are present?
    // How many articles are associated with each dossier category?
    // How many dossier posts exist?
    // How many articles are associated with dossier posts?

    // How do I determine how many posts are associated with dossier
    // posts?

    // We want to display the status of our migration for each element
    // in the list.
    // To this end, we first establish the logical situation, and we
    // then generate the content depending on the logical situation.

    //// Have we generated all required Dossiers?
    // Bool: Test whether, for each dossier category, except 'Dossier'
    // we have have a dossier post.
    //
    // Will also pass when dossier categories have been deleted.

    //// Have we migrated dossier posts?
    // Bool: Test whether we have associated each post currently in a
    // dossier category with that dossier as a 'dossier' post
    //
    // Will also pass when dossier categries have been removed from posts.

    //// Have we removed dossier categories from dossier posts?
    // Bool: Test whether, for each 'dossier' post, we have removed
    // it's 'dossier category'.

    //// Have we removed dossier categories?
    // Bool: Test whether we have removed all dossier categories.

    // Generate the output
    ?>
        <h2>Overview</h2>
        <p>The aim of this plugin is for us to migrate from each
            dossier being a category that is a child of the 'dossiers'
            category, to being a post within the 'dossiers' category
            that is then associated with its content.</p>
        <p>Currently individual articles, for instance, are tagged
            with being part of a specific dossier category, i.e. a
            category that is a child of the 'dossier' category.  After
            this migration each of those articles will instead have
            the 'article' category, but will have an association with
            an post of 'dossier' category.</p>
        <ol>
            <li>Number of Dossier Categories: <?php echo count($categories) ?> [Should be 0 after migration].</li>
            <li>
                Number of articles associated with each dossier category:
                <ol>
                    <?php
                    foreach ( $categories as $category ) {
                        $count = count(get_posts([ 'category' => $category->term_id, 'numberposts' => -1 ]));
                        echo "<li>" . wlst_cat_search($category->term_id, $category->name)
                           . ": " . $count . "</li>";
                        $total_posts += $count;
                    }
                    ?>
                </ol>
            </li>
            <li>
                Number of articles associated with all dossier categories: <?php echo $total_posts ?> [Should be 0 after migration].
            </li>
            <li>
                Number of existing Dossier category posts: <?php echo count($dossiers) ?> [After the migration, this should be the same as Number of Dossier Categories before migration].
            </li>
        </ol>
        <p><a href='?page=wanderlust&operation=generate'>Start migration</a></p>
    </div>
    <?php
}

?>
